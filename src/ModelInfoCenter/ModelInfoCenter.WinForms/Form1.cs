﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ModelInfoCenter.ModelWork;

namespace ModelInfoCenter.WinForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private ModelSettings GetSettings()
        {
            try
            {
                var settings = new ModelSettings();
                settings.Client.ClientComingTime.FromTime = Convert.ToDouble(textBox1.Text);
                settings.Client.ClientComingTime.ToTime = Convert.ToDouble(textBox2.Text);

                settings.Operator.Operator1_ServingClientTime.FromTime = Convert.ToDouble(textBox3.Text);
                settings.Operator.Operator1_ServingClientTime.ToTime = Convert.ToDouble(textBox4.Text);
                settings.Operator.Operator2_ServingClientTime.FromTime = Convert.ToDouble(textBox5.Text);
                settings.Operator.Operator2_ServingClientTime.ToTime = Convert.ToDouble(textBox6.Text);
                settings.Operator.Operator3_ServingClientTime.FromTime = Convert.ToDouble(textBox7.Text);
                settings.Operator.Operator3_ServingClientTime.ToTime = Convert.ToDouble(textBox8.Text);

                settings.Computer.Computer1_ProcessingRequestTime = Convert.ToDouble(textBox9.Text);
                settings.Computer.Computer2_ProcessingRequestTime = Convert.ToDouble(textBox10.Text);

                settings.DeltaT = Convert.ToDouble(textBox11.Text);
                settings.Statistics.UpperLimitOfRequestsNumber = Convert.ToInt32(textBox12.Text);

                return settings;
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка считывания данных.");
            }
            return null;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var settings = GetSettings();
            if (settings != null)
            {
                var model = ModelManager.Instance;
                model.Initialize(settings);
                model.ModelWholeSystem();
                textBox13.Text = Math.Round(model.GetPossobolityOfDenial(), 2).ToString();
            }
            
        }


    }
}
