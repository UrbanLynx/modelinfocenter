﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelInfoCenter.ModelWork;

namespace ModelInfoCenter
{
    public class ModelManager
    {
        #region Members
        
        private static volatile ModelManager _instance;
        private static object _syncRoot = new Object();

        // Scheme
        private Client client;
        private List<Operator> operators;
        private List<Storage> storages;
        private List<Computer> computers; 

        // Model Paameters
        private List<ModelingEvent> modelingEvents;
        private StatisticsModule statistics;
        private double currentTime;
        private double dt;

        #endregion

        #region Constructors
        private ModelManager() { }

        public static ModelManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncRoot)
                    {
                        if (_instance == null)
                            _instance = new ModelManager();
                    }
                    //ModelManager.Instance.Initialize();
                }

                return _instance;
            }
        }

        public void Initialize(ModelSettings settings)
        {
            CreateScheme(settings);
            CreateStatistics(settings);
            ConnectEventsInScheme();
            InitializeParameters(settings);
        }

        private void CreateScheme(ModelSettings settings)
        {
            var computer1 = new Computer(settings.Computer.Computer1_ProcessingRequestTime);
            var computer2 = new Computer(settings.Computer.Computer2_ProcessingRequestTime);
            computers = new List<Computer>() {computer1, computer2};

            var storage1 = new Storage(computer1);
            var storage2 = new Storage(computer2);
            storages = new List<Storage>(){storage1, storage2};

            var operator1 = new Operator(settings.Operator.Operator1_ServingClientTime, storage1);
            var operator2 = new Operator(settings.Operator.Operator2_ServingClientTime, storage1);
            var operator3 = new Operator(settings.Operator.Operator3_ServingClientTime, storage2);
            operators = new List<Operator>(){operator1, operator2, operator3};

            client = new Client(settings.Client.ClientComingTime);
        }

        private void CreateStatistics(ModelSettings settings)
        {
            statistics = new StatisticsModule(settings.Statistics.UpperLimitOfRequestsNumber);
        }

        private void ConnectEventsInScheme()
        {
            client.ClientEvent += AddModelingEvent;
            client.ClientAccepted += statistics.AddAcceptedClient;
            client.ClientDenied += statistics.AddDeniedClient;

            foreach (var op in operators)
            {
                op.OperatorEvent += AddModelingEvent;
            }
            foreach (var computer in computers)
            {
                computer.ComputerEvent += AddModelingEvent;
                computer.RequestProcessed += statistics.RequestProcessed;
            }

        }

        private void InitializeParameters(ModelSettings settings)
        {
            modelingEvents = new List<ModelingEvent>();
            currentTime = 0;
            dt = 0.01;
        }

        #endregion

        #region Methods

        public void ModelWholeSystem()
        {
            client.ProcessNewClient();
            while (statistics.DoContinueModeling())
            {
                ExecuteCurrentEvents();
                currentTime += dt;
            }
        }

        public double GetPossobolityOfDenial()
        {
            return statistics.GetPossibilityOfDenial();
        }

        public List<Operator> GetOperators()
        {
            return operators;
        }

        private void ExecuteCurrentEvents()
        {
            List<ModelingEvent> currentEvents = GetCurrentEvents();
            foreach (var modelingEvent in currentEvents)
            {
                modelingEvent.CallFunction();
                modelingEvents.Remove(modelingEvent);
            }
        }

        private List<ModelingEvent> GetCurrentEvents()
        {
            List<ModelingEvent> currentEvents = modelingEvents.Where(ev => ev.NextCallTime >= currentTime && ev.NextCallTime <= currentTime+dt).ToList();
            return currentEvents;
        }

        public void AddModelingEvent(object sender, ModelingEventArgs modelingEventArgs)
        {
            var modelEvent = modelingEventArgs.ModelEvent;
            modelEvent.NextCallTime += currentTime;
            
            var nearestEventIndex = modelingEvents.FindIndex(ev => modelEvent.NextCallTime < ev.NextCallTime);
            if (nearestEventIndex != -1)
            {
                modelingEvents.Insert(nearestEventIndex, modelEvent);
            }
            else
            {
                modelingEvents.Add(modelEvent);
            }
        }


        #endregion
    }
}
