﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelInfoCenter.ModelWork
{
    public class ModelSettings
    {
        public ModelSettings()
        {
            Client = new ClientSettings();
            Operator = new OperatorSettings();
            Computer = new ComputerSettings();
            Statistics = new StatisticsSettings();
        }

        public ClientSettings Client { get; set; }
        public OperatorSettings Operator { get; set; }
        public ComputerSettings Computer { get; set; }
        public StatisticsSettings Statistics { get; set; }
        public double DeltaT { get; set; }
    }

    public class ClientSettings
    {
        public ClientSettings()
        {
            ClientComingTime = new TimeRange();
        }
         public TimeRange ClientComingTime { get; set; }
    }

    public class OperatorSettings
    {
        public OperatorSettings()
        {
            Operator1_ServingClientTime = new TimeRange();
            Operator2_ServingClientTime = new TimeRange();
            Operator3_ServingClientTime = new TimeRange();
        }

        public TimeRange Operator1_ServingClientTime { get; set; }
        public TimeRange Operator2_ServingClientTime { get; set; }
        public TimeRange Operator3_ServingClientTime { get; set; }
    }

    public class ComputerSettings
    {
         public double Computer1_ProcessingRequestTime { get; set; }
         public double Computer2_ProcessingRequestTime { get; set; }
    }

    public class StatisticsSettings
    {
        public int UpperLimitOfRequestsNumber { get; set; }
    }
}
