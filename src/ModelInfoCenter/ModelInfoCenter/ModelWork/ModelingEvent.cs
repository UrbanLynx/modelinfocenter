﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelInfoCenter
{
    public class ModelingEvent
    {
        public ModelingEvent(Action callFunction, double nextCallTime)
        {
            CallFunction = callFunction;
            NextCallTime = nextCallTime;
        }
        public double NextCallTime { get; set; }
        public Action CallFunction { get; set; }
    }

    public class ModelingEventArgs : EventArgs
    {
        public ModelingEventArgs(Action callFunction, double nextCallTime)
        {
            ModelEvent = new ModelingEvent(callFunction, nextCallTime);
        }
        public ModelingEvent ModelEvent { get; set; }
    }
}
