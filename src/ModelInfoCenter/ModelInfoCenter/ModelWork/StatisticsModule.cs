﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelInfoCenter
{
    class StatisticsModule
    {
        private int maxLimitOfRequests;
        private int numOfProcessedRequests;
        private int numOfAcceptedClients;
        private int numOfDeniedClients;

        public StatisticsModule(int limitOfRequests)
        {
            maxLimitOfRequests = limitOfRequests;
            numOfDeniedClients = 0;
            numOfProcessedRequests = 0;
            numOfAcceptedClients = 0;
        }

        public double GetPossibilityOfDenial()
        {
            var allClients = numOfDeniedClients + numOfAcceptedClients;
            if (allClients != 0)
            {
                return (double) numOfDeniedClients/allClients;
            }
            return 0;
        }

        public void AddAcceptedClient(object sender, EventArgs eventArgs)
        {
            numOfAcceptedClients++;
        }

        public void AddDeniedClient(object sender, EventArgs eventArgs)
        {
            numOfDeniedClients++;
        }

        public void RequestProcessed(object sender, EventArgs eventArgs)
        {
            numOfProcessedRequests++;
        }

        public bool DoContinueModeling()
        {
            return numOfProcessedRequests < maxLimitOfRequests;
        }
    }
}
