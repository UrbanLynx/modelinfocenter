﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelInfoCenter
{
    public class Computer
    {
        private double processTime;
        private bool isFree;

        public event EventHandler<ModelingEventArgs> ComputerEvent;
        public event EventHandler RequestProcessed;
        public Computer(double processingTime)
        {
            processTime = processingTime;
            isFree = true;
        }

        public bool IsFree()
        {
            return isFree;
        }
        
        public void StartProcessRequest(Request request)
        {
            GenerateProcessTime();
            isFree = false;
        }
        public void EndProcessRequest()
        {
            isFree = true;
            RequestProcessed(this, new EventArgs());
        }

        private void GenerateProcessTime()
        {
            ComputerEvent(this, new ModelingEventArgs(EndProcessRequest, processTime));
        }

        
    }
}
