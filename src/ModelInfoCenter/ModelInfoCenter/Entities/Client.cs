﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelInfoCenter.Generator;
using ModelInfoCenter.ModelWork;

namespace ModelInfoCenter
{
    class Client
    {
        private TimeRange clientComeRange;

        public event EventHandler<ModelingEventArgs> ClientEvent;
        public event EventHandler ClientAccepted;
        public event EventHandler ClientDenied;

        public Client(TimeRange clientTimeRange)
        {
            clientComeRange = clientTimeRange;
        }
        
        public void ProcessNewClient()
        {
            ChooseOperator();
            GenerateNextClientComeTime();
        }

        private void GenerateNextClientComeTime()
        {
            double nextTime = RandomGenerator.GetUniform(clientComeRange.FromTime, clientComeRange.ToTime);
            ClientEvent(this, new ModelingEventArgs(ProcessNewClient, nextTime));
        }

        private void ChooseOperator()
        {
            var operators = ModelManager.Instance.GetOperators();
            operators.Sort((x, y) => x.GetNextServeTime().CompareTo(y.GetNextServeTime()));
            var freeAndEffectiveOperator = operators.FirstOrDefault(op => op.IsFree());
            if (freeAndEffectiveOperator != null)
            {
                ClientAccepted(this, new EventArgs());
                freeAndEffectiveOperator.StartService();
            }
            else
            {
                ClientDenied(this,new EventArgs());
            }
        }
    }
}
