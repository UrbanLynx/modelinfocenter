﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelInfoCenter.Generator;
using ModelInfoCenter.ModelWork;

namespace ModelInfoCenter
{
    public class Operator
    {
        private TimeRange serviceTimeRange;
        private bool isFree;
        private Storage storage;
        private double nextServeTime;

        public event EventHandler<ModelingEventArgs> OperatorEvent;

        public Operator(TimeRange serviceRange, Storage opStorage)
        {
            serviceTimeRange = serviceRange;
            storage = opStorage;
            GenerateNextServeTime();
            isFree = true;
        }

        public void StartService()
        {
            StartNextServeTime();
            isFree = false;
        }

        public void EndService()
        {
            isFree = true;
            AddRequestToStorage();
            GenerateNextServeTime();
        }

        public double GetNextServeTime()
        {
            return nextServeTime;
        }

        public bool IsFree()
        {
            return isFree;
        }


        private void AddRequestToStorage()
        {
            storage.AddRequest(new Request());

        }

        private void GenerateNextServeTime()
        {
            nextServeTime = RandomGenerator.GetUniform(serviceTimeRange.FromTime, serviceTimeRange.ToTime);
        }

        private void StartNextServeTime()
        {
            OperatorEvent(this, new ModelingEventArgs(EndService, nextServeTime));
        }
    }
}
