﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelInfoCenter
{
    public class Storage
    {
        private Computer computer;
        private Queue<Request> requests;

        public Storage(Computer storageComputer)
        {
            computer = storageComputer;
            computer.RequestProcessed += ComputerFreed;
            requests = new Queue<Request>();
        }

        public void AddRequest(Request request)
        {
            requests.Enqueue(request);
            SendRequestToComputer();

        }

        public void ComputerFreed(object sender, EventArgs eventArgs)
        {
            SendRequestToComputer();
        }

        private bool IsComputerFree()
        {
            return computer.IsFree();
        }

        private void SendRequestToComputer()
        {
            if (IsComputerFree())
            {
                if (requests.Count > 0)
                {
                    var request = requests.Dequeue();
                    computer.StartProcessRequest(request);
                }
            }
        }
    }
}
